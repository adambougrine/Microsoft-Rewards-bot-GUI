from PyQt5 import QtCore, QtWidgets
from .ui import UserInterface
from pathlib import Path
from .farmer import Farmer
from selenium.webdriver.chrome.webdriver import WebDriver
import webbrowser
import json


class Window(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.ui = UserInterface()
        self.ui.setupUi(self)
        self.ui.start_button.clicked.connect(self.start)
        self.ui.stop_button.clicked.connect(self.stop)
        self.ui.open_accounts_button.clicked.connect(self.open_accounts)
        self.ui.creator_label.linkActivated['QString'].connect(lambda: webbrowser.open("https://github.com/farshadz1997/"))
        self.default_config = {
            "accountsPath": None,
            "time": "12:00 AM",
            "globalOptions": {
                "headless": False,
                "session": False,
                "fast": False,
                "checkInternet": True,
                "saveErrors": False,
                "shutdownSystem": False
            },
            "farmOptions": {
                "dailyQuests": True,
                "punchCards": True,
                "moreActivities": True,
                "MSNShoppingGame": True,
                "searchPC": True,
                "searchMobile": True,
            },
            "telegram": {"sendToTelegram": False, "token": "", "chatID": ""},
        }
        self.sample_accounts = [{"username": "Your Email", "password": "Your Password"}]
        
    def disable_elements(self):
        self.ui.start_button.setEnabled(False)
        self.ui.global_options_groupbox.setEnabled(False)
        self.ui.farm_options_groupbox.setEnabled(False)
        self.ui.open_accounts_button.setEnabled(False)
        self.ui.active_timer_checkbox.setEnabled(False)
        self.ui.send_to_telegram_checkbox.setEnabled(False)
        self.ui.api_key_lineedit.setEnabled(False)
        self.ui.chat_id_lineedit.setEnabled(False)
        self.ui.timeEdit.setEnabled(False)
        self.ui.accounts_count.setText(str(len(self.accounts)))
        self.ui.finished_accounts_count.setText("0")
        self.ui.locked_accounts_count.setText("0")
        self.ui.suspended_accounts_count.setText("0")
        self.ui.current_account.setText("")
        self.ui.current_point.setText("")
        self.ui.section.setText("")
        self.ui.detail.setText("")
        
    def enable_elements(self):
        self.ui.current_account.setText("-")
        self.ui.current_point.setText("-")
        self.ui.section.setText("-")
        self.ui.detail.setText("-")
        self.ui.start_button.setEnabled(True)
        self.ui.global_options_groupbox.setEnabled(True)
        self.ui.farm_options_groupbox.setEnabled(True)
        self.ui.open_accounts_button.setEnabled(True)
        self.ui.active_timer_checkbox.setEnabled(True)
        if not self.ui.active_timer_checkbox.isChecked():
            self.ui.timeEdit.setEnabled(False)
        else:
            self.ui.timeEdit.setEnabled(True)
        self.ui.send_to_telegram_checkbox.setEnabled(True)
        if not self.ui.send_to_telegram_checkbox.isChecked():
            self.ui.api_key_lineedit.setEnabled(False)
            self.ui.chat_id_lineedit.setEnabled(False)
        else:
            self.ui.api_key_lineedit.setEnabled(True)
            self.ui.chat_id_lineedit.setEnabled(True)
    
    def update_points_counter(self, value):
        self.ui.current_point.setText(str(value))
    
    def update_stop_button(self, value: bool):
        self.ui.stop_button.setEnabled(value)
      
    def update_section(self, value):
        self.ui.section.setText(value)
        self.ui.section.adjustSize()
        
    def update_detail(self, value):
        self.ui.detail.setText(value)
        self.ui.detail.adjustSize()
    
    def update_accounts_info(self):
        self.ui.current_account.setText(self.farmer.current_account)
        self.ui.finished_accounts_count.setText(str(len(self.farmer.finished_accounts)))
        self.ui.locked_accounts_count.setText(str(len(self.farmer.locked_accounts)))
        self.ui.suspended_accounts_count.setText(str(len(self.farmer.suspended_accounts)))
    
    def start(self):
        self.save_config()
        if not any(self.config["farmOptions"].values()):
            self.send_error(text="You must select at least one option to farm.")
            return None
        self.disable_elements()
        self.farmer_thread = QtCore.QThread(self.thread())
        self.farmer = Farmer(self)
        self.farmer.moveToThread(self.farmer_thread)
        self.farmer_thread.started.connect(self.farmer.perform_run)
        self.farmer.finished.connect(self.farmer_thread.quit)
        self.farmer.finished.connect(self.farmer.deleteLater)
        self.farmer_thread.finished.connect(self.farmer_thread.deleteLater)
        # update elements with signals
        self.farmer.finished.connect(self.enable_elements)
        self.farmer.finished.connect(lambda: self.ui.stop_button.setEnabled(False))
        self.farmer.points.connect(self.update_points_counter)
        self.farmer.section.connect(self.update_section)
        self.farmer.detail.connect(self.update_detail)
        self.farmer.accounts_info_sig.connect(self.update_accounts_info)
        self.farmer.stop_button_enabled.connect(self.update_stop_button)
        
        self.farmer_thread.start()

    def stop(self):
        self.ui.section.setText("Stopping...")
        self.ui.detail.setText("-")
        self.ui.stop_button.setEnabled(False)
        if isinstance(self.farmer.browser, WebDriver) or self.farmer.browser is not None:
            self.farmer.browser.quit()
        self.farmer_thread.requestInterruption()

    def open_accounts(self):
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(None, "Open File", "", "JSON Files (*.json)")
        if fileName:
            self.accounts = self.get_accounts(fileName)

    def get_accounts_path(self) -> str:
        return self.ui.accounts_lineedit.text()
    
    def get_timer_checkbox_status(self) -> bool:
        return self.ui.active_timer_checkbox.isChecked()
    
    def get_time(self):
        return self.ui.timeEdit.time()
    
    def get_accounts(self, path=Path(__file__).parent.parent / "accounts.json"):
        try:
            accounts = json.load(open(path, "r"))
        except json.decoder.JSONDecodeError as e:
            self.send_error("Error", "JSON decode error", str(e))
        except FileNotFoundError:
            accounts_path = Path(__file__).parent.parent / "accounts.json"
            with open(accounts_path, "w") as f:
                f.write(json.dumps(self.sample_accounts, indent=4))
            self.send_info(
                "Info",
                "accounts file not found",
                "accounts file not found," f"a new one has been created at '{str(accounts_path)}'. Edit it with your infos then open it in Farmer.",
            )
            self.accounts = self.sample_accounts
        else:
            for account in accounts:
                if "username" and "password" in account.keys():
                    continue
                else:
                    self.send_error(
                        "Error",
                        "Accounts need to have 'username' and 'password'",
                        "One or some of your accounts do not have username or password.",
                    )
                    self.ui.accounts_lineedit.clear()
                    return None
            self.ui.accounts_lineedit.setText(str(path))
            self.ui.start_button.setEnabled(True)
            self.accounts = accounts
            return accounts

    def send_error(self, window_title: str = "Error", text: str = None, detail: str = None):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Critical)
        msg.setWindowTitle(window_title)
        msg.setText(text)
        msg.setInformativeText(detail)
        msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
        msg.exec_()

    def send_info(self, window_title: str = "Info", text: str = None, detail: str = None):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Information)
        msg.setWindowTitle(window_title)
        msg.setText(text)
        msg.setInformativeText(detail)
        msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
        msg.exec_()
        
    def validate_and_sync_config(self) -> None:
        """Check if config.json is up to date with default_config"""
        need_sync = False
        for key, value in self.default_config.items():
            if key not in self.config.keys() and type(key) != dict:
                need_sync = True
                self.config[key] = self.default_config[key]
            if type(value) == dict:
                if self.config[key].keys() == value.keys():
                    continue
                else:
                    need_sync = True
                    diffs = list(value.keys() - self.config[key].keys())
                    for diff in diffs:
                        self.config[key][diff] = value[diff]
        if need_sync:
            with open(f"{Path(__file__).parent.parent}/config.json", "w") as f:
                f.write(json.dumps(self.config, indent=4))

    def get_config(self):
        """Get config from config.json or create it if it doesn't exist"""
        try:
            config = json.load(open(f"{Path(__file__).parent.parent}/config.json", "r"))
        except (FileNotFoundError, json.decoder.JSONDecodeError):
            with open(f"{Path(__file__).parent.parent}/config.json", "w") as f:
                f.write(json.dumps(self.default_config, indent=4))
            self.config = self.default_config
        else:
            self.config = config
        finally:
            self.validate_and_sync_config()
            if self.config.get("accountsPath") is not None:
                if Path(self.config.get("accountsPath")).is_file():
                    self.accounts = self.get_accounts(config["accountsPath"])
                else:
                    self.get_accounts()
            else:
                self.get_accounts()
            return self.config

    def set_config(self):
        """Set config from config.json"""
        self.ui.timeEdit.setTime(QtCore.QTime.fromString(self.config["time"], "hh:mm AP"))
        self.ui.headless_checkbox.setChecked(self.config["globalOptions"]["headless"])
        self.ui.session_checkbox.setChecked(self.config["globalOptions"]["session"])
        self.ui.fast_mode_checkbox.setChecked(self.config["globalOptions"]["fast"])
        self.ui.check_internet_checkbox.setChecked(self.config["globalOptions"]["checkInternet"])
        self.ui.save_errors.setChecked(self.config["globalOptions"]["saveErrors"])
        self.ui.shutdown_system.setChecked(self.config["globalOptions"]["shutdownSystem"])
        self.ui.daily_quests_checkbox.setChecked(self.config["farmOptions"]["dailyQuests"])
        self.ui.punch_cards_checkbox.setChecked(self.config["farmOptions"]["punchCards"])
        self.ui.more_activities_checkbox.setChecked(self.config["farmOptions"]["moreActivities"])
        self.ui.msn_shopping_game_checkbox.setChecked(self.config["farmOptions"]["MSNShoppingGame"])
        self.ui.search_pc_checkbox.setChecked(self.config["farmOptions"]["searchPC"])
        self.ui.search_mobile_checkbox.setChecked(self.config["farmOptions"]["searchMobile"])
        self.ui.api_key_lineedit.setText(self.config["telegram"]["token"])
        self.ui.chat_id_lineedit.setText(self.config["telegram"]["chatID"])
        self.ui.send_to_telegram_checkbox.setChecked(self.config["telegram"]["sendToTelegram"])
        if self.config["telegram"]["sendToTelegram"]:
            self.ui.api_key_lineedit.setEnabled(True)
            self.ui.chat_id_lineedit.setEnabled(True)
            
    def save_config(self):
        """Save config to config.json"""
        self.config = {
            "accountsPath": self.ui.accounts_lineedit.text(),
            "time": self.ui.timeEdit.text(),
            "globalOptions": {
                "headless": self.ui.headless_checkbox.isChecked(),
                "session": self.ui.session_checkbox.isChecked(),
                "fast": self.ui.fast_mode_checkbox.isChecked(),
                "checkInternet": self.ui.check_internet_checkbox.isChecked(),
                "saveErrors": self.ui.save_errors.isChecked(),
                "shutdownSystem": self.ui.shutdown_system.isChecked(),
            },
            "farmOptions": {
                "dailyQuests": self.ui.daily_quests_checkbox.isChecked(),
                "punchCards": self.ui.punch_cards_checkbox.isChecked(),
                "moreActivities": self.ui.more_activities_checkbox.isChecked(),
                "MSNShoppingGame": self.ui.msn_shopping_game_checkbox.isChecked(),
                "searchPC": self.ui.search_pc_checkbox.isChecked(),
                "searchMobile": self.ui.search_mobile_checkbox.isChecked(),
            },
            "telegram": {
                "sendToTelegram": self.ui.send_to_telegram_checkbox.isChecked(),
                "token": self.ui.api_key_lineedit.text(),
                "chatID": self.ui.chat_id_lineedit.text()
                },
        }
        with open(f"{Path(__file__).parent.parent}/config.json", "w") as f:
            f.write(json.dumps(self.config, indent=4))