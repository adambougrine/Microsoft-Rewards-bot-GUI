import json
import os
import platform
import random
from functools import wraps
import subprocess
import time
import urllib.parse
from urllib3.exceptions import MaxRetryError, NewConnectionError
from pathlib import Path
from datetime import date, datetime, timedelta
from notifiers import get_notifier
from PyQt5.QtCore import QObject, pyqtSignal, QTime
import copy
from typing import List, Union
from .exceptions import *

import ipapi
import requests
from func_timeout import FunctionTimedOut, func_set_timeout
from random_word import RandomWords
from selenium import webdriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.common.exceptions import (ElementNotInteractableException,
                                        NoAlertPresentException,
                                        NoSuchElementException,
                                        SessionNotCreatedException,
                                        TimeoutException,
                                        UnexpectedAlertPresentException,
                                        InvalidSessionIdException,
                                        JavascriptException,
                                        ElementNotVisibleException)
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.chrome.options import Options


def retry_on_500_errors(function):
    @wraps(function)
    def wrapper(*args, **kwargs):
        farmer: Farmer = args[0]
        driver: WebDriver = farmer.browser
        error_codes = ["HTTP ERROR 500", "HTTP ERROR 502", "HTTP ERROR 503", "HTTP ERROR 504", "HTTP ERROR 505"]
        status_code = "-"
        result = function(*args, **kwargs)
        while True:
            try:
                status_code = driver.execute_script("return document.readyState;")
                if status_code == "complete" and not any(error_code in driver.page_source for error_code in error_codes):
                    return result
                elif status_code == "loading":
                    return result
                else:
                    raise Exception("Page not loaded")
            except Exception as e:
                if any(error_code in driver.page_source for error_code in error_codes): # Check if the page contains 500 errors
                    driver.refresh() # Recursively refresh
                else:
                    raise Exception(f"another exception occurred during handling 500 errors with status '{status_code}': {e}")
    return wrapper


class Farmer(QObject):
    finished = pyqtSignal()
    points = pyqtSignal(int)
    section = pyqtSignal(str)
    detail = pyqtSignal(str)
    stop_button_enabled = pyqtSignal(bool)
    accounts_info_sig = pyqtSignal()
    
    PC_USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36 Edg/112.0.1722.58'
    MOBILE_USER_AGENT = 'Mozilla/5.0 (Linux; Android 12; SM-N9750) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Mobile Safari/537.36 EdgA/112.0.1722.46'
    base_url = "https://rewards.bing.com/"
    
    def __init__(self, ui):
        super(Farmer, self).__init__()
        from .ui_functions import Window
        self.ui: Window = ui
        self.accounts = self.ui.accounts
        self.config: dict = self.ui.config
        self.accounts_path = Path(self.ui.get_accounts_path())
        self.browser: WebDriver = None
        self.points_counter: int = 0
        self.finished_accounts: list = []
        self.locked_accounts: list = []
        self.suspended_accounts: list = []
        self.current_account: str = None
        self.browser: WebDriver = None
        self.starting_points: int = None
        self.point_counter: int = 0
        self.logs: dict = {}
        # self.get_or_create_logs()
        self.lang, self.geo, self.tz = self.get_ccode_lang_and_offset()
    
    def create_message(self):
        """Create message from logs to send to Telegram"""
        today = date.today().strftime("%d/%m/%Y")
        total_earned = 0
        total_overall = 0
        message = f'📅 Daily report {today}\n\n'
        for index, value in enumerate(self.logs.items(), 1):
            redeem_message = None
            if value[1].get("Redeem goal title", None):
                redeem_title = value[1].get("Redeem goal title", None)
                redeem_price = value[1].get("Redeem goal price")
                redeem_count = value[1]["Points"] // redeem_price
                if redeem_count > 1:
                    redeem_message = f"🎁 Ready to redeem: {redeem_title} for {redeem_price} points ({redeem_count}x)\n\n"
                else:
                    redeem_message = f"🎁 Ready to redeem: {redeem_title} for {redeem_price} points\n\n"
            if value[1]['Last check'] == str(date.today()):
                status = '✅ Farmed'
                new_points = value[1]["Today's points"]
                total_earned += new_points
                total_points = value[1]["Points"]
                total_overall += total_points
                message += f"{index}. {value[0]}\n📝 Status: {status}\n⭐️ Earned points: {new_points}\n🏅 Total points: {total_points}\n"
                if redeem_message:
                    message += redeem_message
                else:
                    message += "\n"
            elif value[1]['Last check'] == 'Your account has been suspended':
                status = '❌ Suspended'
                message += f"{index}. {value[0]}\n📝 Status: {status}\n\n"
            elif value[1]['Last check'] == 'Your account has been locked !':
                status = '⚠️ Locked'
                message += f"{index}. {value[0]}\n📝 Status: {status}\n\n"
            elif value[1]['Last check'] == 'Unusual activity detected !':
                status = '⚠️ Unusual activity detected'
                message += f"{index}. {value[0]}\n📝 Status: {status}\n\n"
            elif value[1]['Last check'] == 'Unknown error !':
                status = '⛔️ Unknown error occurred'
                message += f"{index}. {value[0]}\n📝 Status: {status}\n\n"
            else:
                status = f'Farmed on {value[1]["Last check"]}'
                new_points = value[1]["Today's points"]
                total_earned += new_points
                total_points = value[1]["Points"]
                total_overall += total_points
                message += f"{index}. {value[0]}\n📝 Status: {status}\n⭐️ Earned points: {new_points}\n🏅 Total points: {total_points}\n"
                if redeem_message:
                    message += redeem_message
                else:
                    message += "\n"
        message += f"💵 Total earned points: {total_earned} (${total_earned/1300:0.02f}) (€{total_earned/1500:0.02f})"
        message += f"\n💵 Total points overall: {total_overall} (${total_overall/1300:0.02f}) (€{total_overall/1500:0.02f})"
        return message

    def send_report_to_telegram(self, message):
        t = get_notifier('telegram') 
        if len(message) > 4096:
            messages = [message[i:i+4096] for i in range(0, len(message), 4096)]
            for ms in messages:
                t.notify(message=ms, token=self.config["telegram"]["token"], chat_id=self.config["telegram"]["chatID"])
        else:
            t.notify(message=message, token=self.config["telegram"]["token"], chat_id=self.config["telegram"]["chatID"])
    
    def check_internet_connection(self):
        system = platform.system()
        while True and self.config["globalOptions"]["checkInternet"]:
            try:
                if self.ui.farmer_thread.isInterruptionRequested():
                    self.finished.emit()
                    self.ui.enable_elements()
                    return False
                if system == "Windows":
                    si = subprocess.STARTUPINFO()
                    si.dwFlags |= subprocess.STARTF_USESHOWWINDOW
                    subprocess.check_output(["ping", "-n", "1", "8.8.8.8"], timeout=5, startupinfo=si)
                elif system == "Linux":
                    subprocess.check_output(["ping", "-c", "1", "8.8.8.8"], timeout=5)
                self.section.emit("-")
                self.detail.emit("-")
                return True
            except (subprocess.CalledProcessError, subprocess.TimeoutExpired):
                self.section.emit("No internet connection...")
                self.detail.emit("Checking...")
                time.sleep(1)
        return True
    
    def get_or_create_logs(self):
        """Read logs and check whether account farmed or not"""
        shared_items =[]
        try:
            self.logs = json.load(open(f"{self.accounts_path.parent}/Logs_{self.accounts_path.stem}.txt", "r"))
            # delete Time period from logs
            self.logs.pop("Elapsed time", None)
            # sync accounts and logs file for new accounts or remove accounts from logs.
            for user in self.accounts:
                shared_items.append(user['username'])
                if not user['username'] in self.logs.keys():
                    self.logs[user["username"]] = {"Last check": "",
                                            "Today's points": 0,
                                            "Points": 0}
            if shared_items != self.logs.keys():
                diff = self.logs.keys() - shared_items
                for accs in list(diff):
                    del self.logs[accs]
            
            # check that if any of accounts has farmed today or not.
            for account in self.logs.keys():
                if self.logs[account]["Last check"] == str(date.today()) and list(self.logs[account].keys()) == ['Last check',
                                                                                                                "Today's points",
                                                                                                                'Points']:
                    self.finished_accounts.append(account)
                elif self.logs[account]['Last check'] == 'Your account has been suspended':
                    self.suspended_accounts.append(account)
                elif self.logs[account]['Last check'] == str(date.today()) and list(self.logs[account].keys()) == ['Last check', 
                                                                                                                   "Today's points", 
                                                                                                                   'Points',
                                                                                                                   'Daily',
                                                                                                                   'Punch cards',
                                                                                                                   'MSN shopping game',
                                                                                                                   'More promotions',
                                                                                                                   'PC searches',
                                                                                                                   'Mobile searches']:
                    if not self.does_account_need_farm(account):
                        self.current_account = account
                        self.clean_logs()
                        self.finished_accounts.append(account)
                        self.current_account = None
                    else:
                        continue
                else:
                    self.logs[account]['Daily'] = False
                    self.logs[account]['Punch cards'] = False
                    self.logs[account]['More promotions'] = False
                    self.logs[account]['MSN shopping game'] = False
                    self.logs[account]['PC searches'] = False 
                    self.logs[account]['Mobile searches'] = False
                if not isinstance(self.logs[account]["Points"], int):
                    self.logs[account]['Points'] = 0
        except (FileNotFoundError, json.decoder.JSONDecodeError):
            for account in self.accounts:
                self.logs[account["username"]] = {"Last check": "",
                                            "Today's points": 0,
                                            "Points": 0,
                                            "Daily": False,
                                            "Punch cards": False,
                                            "More promotions": False,
                                            "MSN shopping game": False,
                                            "PC searches": False,
                                            "Mobile searches": False}
        finally:
            self.update_logs()
            self.accounts_info_sig.emit()
        
    def update_logs(self):
        """Update logs with new data"""
        logs = copy.deepcopy(self.logs)
        for account in logs:
            if account == "Elapsed time": continue
            logs[account].pop("Redeem goal title", None)
            logs[account].pop("Redeem goal price", None)
        with open(f'{self.accounts_path.parent}/Logs_{self.accounts_path.stem}.txt', 'w') as file:
            file.write(json.dumps(logs, indent = 4))

    def clean_logs(self):
        """Delete Daily, Punch cards, More promotions, PC searches and Mobile searches from logs"""
        self.logs[self.current_account].pop("Daily", None)
        self.logs[self.current_account].pop("Punch cards", None)
        self.logs[self.current_account].pop("More promotions", None)
        self.logs[self.current_account].pop("MSN shopping game", None)
        self.logs[self.current_account].pop("PC searches", None)
        self.logs[self.current_account].pop("Mobile searches", None)
    
    def is_pc_need(self) -> bool:
        """Check if browser for PC is needed or not based on farm options and account status"""
        if self.config["farmOptions"]["dailyQuests"] and self.logs[self.current_account]["Daily"] == False:
            return True
        elif self.config["farmOptions"]["punchCards"] and self.logs[self.current_account]["Punch cards"] == False:
            return True
        elif self.config["farmOptions"]["moreActivities"] and self.logs[self.current_account]["More promotions"] == False:
            return True
        elif self.config["farmOptions"]["MSNShoppingGame"] and self.logs[self.current_account]["MSN shopping game"] == False:
            return True
        elif self.config["farmOptions"]["searchPC"] and self.logs[self.current_account]["PC searches"] == False:
            return True
        else:
            return False
    
    def does_account_need_farm(self, account: str):
        """Check that does the account need to be farmed based on logs and options"""
        conditions = []
        if self.config["farmOptions"]["dailyQuests"] and not self.logs[account]["Daily"]:
            conditions.append(True)
        if self.config["farmOptions"]["punchCards"] and not self.logs[account]["Punch cards"]:
            conditions.append(True)
        if self.config["farmOptions"]["moreActivities"] and not self.logs[account]["More promotions"]:
            conditions.append(True)
        if self.config["farmOptions"]["searchPC"] and not self.logs[account]["PC searches"]:
            conditions.append(True)
        if self.config["farmOptions"]["searchMobile"] and not self.logs[account]["Mobile searches"]:
            conditions.append(True)
        if any(conditions):
            return True
        else:
            return False
    
    def is_element_exists(self, _by: By, element: str) -> bool:
        '''Returns True if given element exists else False'''
        try:
            self.browser.find_element(_by, element)
        except NoSuchElementException:
            return False
        return True
    
    def find_between(self, s: str, first: str, last: str) -> str:
        try:
            start = s.index(first) + len(first)
            end = s.index(last, start)
            return s[start:end]
        except ValueError:
            return ""
    
    def is_proxy_working(self, proxy: str):
        '''Check if proxy is working or not'''
        try:
            requests.get("https://www.google.com/", proxies={"https": proxy}, timeout=5)
            return True
        except:
            return False
    
    def browser_setup(self, isMobile: bool = False, user_agent = PC_USER_AGENT, proxy: str = None):
        # Create Chrome browser
        options = Options()
        if self.config["globalOptions"]["session"]:
            if not isMobile:
                options.add_argument(f'--user-data-dir={self.accounts_path.parent}/Profiles/{self.current_account}/PC')
            else:
                options.add_argument(f'--user-data-dir={self.accounts_path.parent}/Profiles/{self.current_account}/Mobile')
        options.add_argument("user-agent=" + user_agent)
        options.add_argument('lang=' + self.lang.split("-")[0])
        options.add_argument('--disable-blink-features=AutomationControlled')
        prefs = {"profile.default_content_setting_values.geolocation": 2,
                "credentials_enable_service": False,
                "profile.password_manager_enabled": False,
                "webrtc.ip_handling_policy": "disable_non_proxied_udp",
                "webrtc.multiple_routes_enabled": False,
                "webrtc.nonproxied_udp_enabled" : False}
        options.add_experimental_option("prefs", prefs)
        options.add_experimental_option("useAutomationExtension", False)
        options.add_experimental_option("excludeSwitches", ["enable-automation"])
        if self.config.get("globalOptions", False).get("headless", False):
            options.add_argument("--headless")
        if proxy:
            if self.is_proxy_working(proxy):
                options.add_argument(f'--proxy-server={proxy}')
        options.add_argument('log-level=3')
        options.add_argument("--start-maximized")
        chrome_service = ChromeService()
        if platform.system() == 'Linux':
            options.add_argument("--no-sandbox")
            options.add_argument("--disable-dev-shm-usage")
        if platform.system() == 'Windows':
            chrome_service.creationflags = subprocess.CREATE_NO_WINDOW
        chrome_browser_obj = webdriver.Chrome(options=options, service=chrome_service)
        self.browser = chrome_browser_obj
        return self.browser
    
    @retry_on_500_errors
    def go_to_url(self, url: str):
        self.browser.get(url)
    
    def login(self, email: str, pwd: str, isMobile: bool = False):
        """Login into  Microsoft account"""
        
        def wait_until_blank_page_load():
            wait = WebDriverWait(self.browser, 10)
            wait.until(ec.presence_of_element_located((By.TAG_NAME, "body")))
            wait.until(ec.presence_of_all_elements_located)
            wait.until(ec.title_contains(""))
            wait.until(ec.presence_of_element_located((By.CSS_SELECTOR, "html[lang]")))
            wait.until(lambda driver: driver.execute_script("return document.readyState") == "complete")
        
        def accept_privacy():
            time.sleep(3)
            self.wait_until_visible(By.ID, "id__0", 15)
            self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            self.wait_until_clickable(By.ID, "id__0", 15)
            self.browser.find_element(By.ID, "id__0").click()
            WebDriverWait(self.browser, 25).until_not(ec.visibility_of_element_located((By.ID, "id__0")))
            time.sleep(5)
            
        def accept_terms():
            time.sleep(2)
            self.browser.find_element(By.ID, 'iNext').click()
            time.sleep(5)
            
        def answer_break_free_from_password():
            time.sleep(2)
            self.browser.find_element(By.ID, "iCancel").click()
            time.sleep(5)
        
        login_message = "Logging in..." if not isMobile else "Logging in mobile..."
        self.section.emit(login_message)
        # Close welcome tab for new sessions
        if self.config["globalOptions"]["session"]:
            time.sleep(2)
            if len(self.browser.window_handles) > 1:
                current_window = self.browser.current_window_handle
                for handler in self.browser.window_handles:
                    if handler != current_window:
                        self.browser.switch_to.window(handler)
                        time.sleep(0.5)
                        self.browser.close()
                self.browser.switch_to.window(current_window)
        # Access to bing.com
        self.go_to_url('https://login.live.com/')
        # Check if account is already logged in
        if self.config["globalOptions"]["session"]:
            time.sleep(5)
            if self.browser.title == "Microsoft account privacy notice" or self.is_element_exists(By.XPATH, '//*[@id="interruptContainer"]/div[3]/div[3]/img'):
                accept_privacy()
            if self.browser.title == "":
                wait_until_blank_page_load()
            if self.browser.title == "We're updating our terms" or self.is_element_exists(By.ID, 'iAccrualForm'):
                accept_terms()
            self.answer_to_security_info_update()
            # Click No thanks on break free from password question
            if self.is_element_exists(By.ID, "setupAppDesc"):
                answer_break_free_from_password()
            if self.browser.title == 'Microsoft account | Home' or self.is_element_exists(By.ID, 'navs_container'):
                self.detail.emit("Microsoft Rewards...")
                self.rewards_login()
                self.detail.emit("Bing...")
                self.check_bing_login(isMobile)
                return
            elif self.browser.title == 'Your account has been temporarily suspended':
                raise AccountLockedException('Your account has been locked !')
            elif self.is_element_exists(By.ID, 'mectrl_headerPicture') or 'Sign In or Create' in self.browser.title:
                self.browser.find_element(By.ID, 'mectrl_headerPicture').click()
                self.wait_until_visible(By.ID, 'i0118', 10)
                if self.is_element_exists(By.ID, 'i0118'):
                    self.browser.find_element(By.ID, "i0118").send_keys(pwd)
                    time.sleep(2)
                    self.browser.find_element(By.ID, 'idSIButton9').click()
                    time.sleep(5)
                    self.answer_to_security_info_update()
                    self.section.emit("Logged in")
                    self.detail.emit("Microsoft Rewards...")
                    self.rewards_login()
                    self.detail.emit("Bing...")
                    self.check_bing_login(isMobile)
                    return None
        # Wait complete loading
        self.wait_until_visible(By.ID, 'loginHeader', 10)
        # Enter email
        self.browser.find_element(By.NAME, "loginfmt").send_keys(email)
        # Click next
        self.browser.find_element(By.ID, 'idSIButton9').click()
        # Wait 2 seconds
        time.sleep(5 if not self.config["globalOptions"]["fast"] else 2)
        # Wait complete loading
        self.wait_until_visible(By.ID, 'loginHeader', 10)
        # Enter password
        self.browser.find_element(By.ID, "i0118").send_keys(pwd)
        # Click next
        self.browser.find_element(By.ID, 'idSIButton9').click()
        # Wait 5 seconds
        time.sleep(5)
        try:
            if self.config["globalOptions"]["session"]:
                # Click Yes to stay signed in.
                self.browser.find_element(By.ID, 'idSIButton9').click()
            else:
                # Click No.
                self.browser.find_element(By.ID, 'idBtn_Back').click()
            if self.browser.title == "Microsoft account privacy notice" or self.is_element_exists(By.XPATH, '//*[@id="interruptContainer"]/div[3]/div[3]/img'):
                accept_privacy()
            if self.browser.title == "":
                wait_until_blank_page_load()
            if self.browser.title == "We're updating our terms" or self.is_element_exists(By.ID, 'iAccrualForm'):
                accept_terms()
            self.answer_to_security_info_update()
            # Click No thanks on break free from password question
            if self.is_element_exists(By.ID, "setupAppDesc"):
                answer_break_free_from_password()
        except NoSuchElementException:
            # Check for if account has been locked.
            if self.browser.title == "Your account has been temporarily suspended" or self.is_element_exists(By.CLASS_NAME, "serviceAbusePageContainer  PageContainer"):
                raise AccountLockedException("Your account has been locked !")
            elif self.browser.title == "Help us protect your account":
                raise UnusualActivityException("Unusual activity detected")
            else:
                raise UnhandledException('Unknown error !')
        # Wait 5 seconds
        time.sleep(5)
        # Click Security Check
        try:
            self.browser.find_element(By.ID, 'iLandingViewAction').click()
        except (NoSuchElementException, ElementNotInteractableException) as e:
            pass
        # Wait complete loading
        try:
            self.wait_until_visible(By.ID, 'KmsiCheckboxField', 10)
        except (TimeoutException) as e:
            pass
        # Click next
        try:
            self.browser.find_element(By.ID, 'idSIButton9').click()
            # Wait 5 seconds
            time.sleep(5)
        except (NoSuchElementException, ElementNotInteractableException) as e:
            pass
        # Check Microsoft Rewards
        self.detail.emit("Microsoft Rewards...")
        self.rewards_login()
        # Check Login
        self.detail.emit("Bing...")
        self.check_bing_login(isMobile)

    def answer_to_security_info_update(self):
        """Clicks on looks good if it asks for security info update"""
        if self.browser.title == 'Is your security info still accurate?' or self.is_element_exists(By.ID, 'iLooksGood'):
            time.sleep(2)
            self.browser.find_element(By.ID, 'iLooksGood').click()
            time.sleep(5)
    
    def rewards_login(self):
        """Login into Microsoft rewards and check account"""
        self.go_to_url(self.base_url)
        self.answer_to_security_info_update()
        try:
            time.sleep(10 if not self.config["globalOptions"]["fast"] else 5)
            # click on sign up button if needed
            if self.is_element_exists(By.ID, "start-earning-rewards-link"):
                self.browser.find_element(By.ID, "start-earning-rewards-link").click()
                time.sleep(5)
                self.answer_to_security_info_update()
                self.browser.refresh()
                time.sleep(5)
        except:
            pass
        time.sleep(10 if not self.config["globalOptions"]["fast"] else 5)
        # Check for ErrorMessage in Microsoft rewards page
        try:
            self.browser.find_element(By.ID, 'error').is_displayed()
            if self.browser.find_element(By.XPATH, '//*[@id="error"]/h1').get_attribute('innerHTML') == ' Uh oh, it appears your Microsoft Rewards account has been suspended.':
                raise AccountSuspendedException('Your account has been suspended !')
            elif self.browser.find_element(By.XPATH, '//*[@id="error"]/h1').get_attribute('innerHTML') == 'Microsoft Rewards is not available in this country or region.':
                raise RegionException('Microsoft Rewards is not available in your region !')
        except NoSuchElementException:
            pass

    @func_set_timeout(300)
    def check_bing_login(self, isMobile: bool = False):
        self.go_to_url('https://bing.com/')
        time.sleep(15 if not self.config["globalOptions"]["fast"] else 5)
        # try to get points at first if account already logged in
        if self.config["globalOptions"]["session"]:
            try:
                if not isMobile:
                    try:
                        self.points_counter = int(self.browser.find_element(By.ID, 'id_rc').get_attribute('innerHTML'))
                    except ValueError:
                        if self.browser.find_element(By.ID, 'id_s').is_displayed():
                            self.browser.find_element(By.ID, 'id_s').click()
                            time.sleep(15)
                            self.check_bing_login(isMobile)
                        time.sleep(2)
                        self.points_counter = int(self.browser.find_element(By.ID, "id_rc").get_attribute("innerHTML").replace(",", ""))
                else:
                    self.browser.find_element(By.ID, 'mHamburger').click()
                    time.sleep(1)
                    self.points_counter = int(self.browser.find_element(By.ID, 'fly_id_rc').get_attribute('innerHTML'))
            except:
                pass
            else:
                return None
        #Accept Cookies
        try:
            self.browser.find_element(By.ID, 'bnp_btn_accept').click()
        except:
            pass
        if isMobile:
            # close bing app banner
            if self.is_element_exists(By.ID, 'bnp_rich_div'):
                try:
                    self.browser.find_element(By.XPATH, '//*[@id="bnp_bop_close_icon"]/img').click()
                except NoSuchElementException:
                    pass
            try:
                time.sleep(1)
                self.browser.find_element(By.ID, 'mHamburger').click()
            except:
                try:
                    self.browser.find_element(By.ID, 'bnp_btn_accept').click()
                except:
                    pass
                time.sleep(1)
                if self.is_element_exists(By.XPATH, '//*[@id="bnp_ttc_div"]/div[1]/div[2]/span'):
                    self.browser.execute_script("""var element = document.evaluate('/html/body/div[1]', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
                                            element.remove();""")
                    time.sleep(5)
                time.sleep(1)
                try:
                    self.browser.find_element(By.ID, 'mHamburger').click()
                except:
                    pass
            try:
                time.sleep(1)
                self.browser.find_element(By.ID, 'HBSignIn').click()
            except:
                pass
            try:
                time.sleep(2)
                self.browser.find_element(By.ID, 'iShowSkip').click()
                time.sleep(3)
            except:
                if str(self.browser.current_url).split('?')[0] == "https://account.live.com/proofs/Add":
                    self.finished_accounts.append(self.current_account)
                    self.logs[self.current_account]['Last check'] = 'Requires manual check!'
                    self.update_logs()
                    raise Exception
        time.sleep(5)
        # Refresh page
        self.go_to_url('https://bing.com/')
        time.sleep(15 if not self.config["globalOptions"]["fast"] else 5)
        #Update Counter
        try:
            if not isMobile:
                try:
                    self.points_counter = int(self.browser.find_element(By.ID, 'id_rc').get_attribute('innerHTML'))
                except:
                    if self.browser.find_element(By.ID, 'id_s').is_displayed():
                        self.browser.find_element(By.ID, 'id_s').click()
                        time.sleep(15)
                        self.check_bing_login(isMobile)
                    time.sleep(5)
                    self.points_counter = int(self.browser.find_element(By.ID, "id_rc").get_attribute("innerHTML").replace(",", ""))
            else:
                try:
                    self.browser.find_element(By.ID, 'mHamburger').click()
                except:
                    try:
                        self.browser.find_element(By.ID, 'bnp_close_link').click()
                        time.sleep(4)
                        self.browser.find_element(By.ID, 'bnp_btn_accept').click()
                    except:
                        pass
                    time.sleep(1)
                    self.browser.find_element(By.ID, 'mHamburger').click()
                time.sleep(1)
                self.points_counter = int(self.browser.find_element(By.ID, 'fly_id_rc').get_attribute('innerHTML'))
        except:
            self.check_bing_login(isMobile)
            
    def wait_until_visible(self, by_: By, selector: str, time_to_wait: int = 10):
        WebDriverWait(self.browser, time_to_wait).until(ec.visibility_of_element_located((by_, selector)))

    def wait_until_clickable(self, by_: By, selector: str, time_to_wait: int = 10):
        WebDriverWait(self.browser, time_to_wait).until(ec.element_to_be_clickable((by_, selector)))

    def wait_until_question_refresh(self):
        tries = 0
        refreshCount = 0
        while True:
            try:
                self.browser.find_elements(By.CLASS_NAME, 'rqECredits')[0]
                return True
            except:
                if tries < 10:
                    tries += 1
                    time.sleep(0.5)
                else:
                    if refreshCount < 5:
                        self.browser.refresh()
                        refreshCount += 1
                        tries = 0
                        time.sleep(5)
                    else:
                        return False

    def wait_until_quiz_loads(self):
        tries = 0
        refreshCount = 0
        while True:
            try:
                self.browser.find_element(By.XPATH, '//*[@id="currentQuestionContainer"]')
                return True
            except:
                if tries < 10:
                    tries += 1
                    time.sleep(0.5)
                else:
                    if refreshCount < 5:
                        self.browser.refresh()
                        refreshCount += 1
                        tries = 0
                        time.sleep(5)
                    else:
                        return False

    def get_dashboard_data(self) -> dict:
        tries = 0
        dashboard = None
        while not dashboard and tries <= 5:
            try:
                dashboard = self.find_between(
                    self.browser.find_element(By.XPATH, '/html/body').get_attribute('innerHTML'),
                    "var dashboard = ",
                    ";\n        appDataModule.constant(\"prefetchedDashboard\", dashboard);"
                )
                dashboard = json.loads(dashboard)
            except:
                tries += 1
                if tries == 6:
                    raise Exception("Could not locate dashboard")
                self.browser.refresh()
                self.wait_until_visible(By.ID, 'app-host', 30)
        return dashboard
    
    def get_account_points(self) -> int:
        return self.get_dashboard_data()['userStatus']['availablePoints']
    
    def get_redeem_goal(self):
        user_status = self.get_dashboard_data()["userStatus"]
        return (user_status["redeemGoal"]["title"], user_status["redeemGoal"]["price"])
    
    def get_ccode_lang_and_offset(self) -> tuple:
        try:
            nfo = ipapi.location()
            lang = nfo['languages'].split(',')[0]
            geo = nfo['country']
            tz = str(round(int(nfo['utc_offset']) / 100 * 60))
            return(lang, geo, tz)
        except:
            return('en-US', 'US', '-480')
        
    def get_google_trends(self, numberOfwords: int) -> list:
        search_terms = []
        i = 0
        while len(search_terms) < numberOfwords :
            i += 1
            r = requests.get('https://trends.google.com/trends/api/dailytrends?hl=' + self.lang + '&ed=' + str((date.today() - timedelta(days = i)).strftime('%Y%m%d')) + '&geo=' + self.geo + '&ns=15')
            google_trends = json.loads(r.text[6:])
            for topic in google_trends['default']['trendingSearchesDays'][0]['trendingSearches']:
                search_terms.append(topic['title']['query'].lower())
                for related_topic in topic['relatedQueries']:
                    search_terms.append(related_topic['query'].lower())
            search_terms = list(set(search_terms))
        del search_terms[numberOfwords:(len(search_terms)+1)]
        return search_terms
    
    def get_related_terms(self, word: str) -> list:
        try:
            r = requests.get('https://api.bing.com/osjson.aspx?query=' + word, headers = {'User-agent': self.PC_USER_AGENT})
            return r.json()[1]
        except:
            return []
        
    def reset_tabs(self):
        try:
            curr = self.browser.current_window_handle

            for handle in self.browser.window_handles:
                if handle != curr:
                    self.browser.switch_to.window(handle)
                    time.sleep(0.5)
                    self.browser.close()
                    time.sleep(0.5)

            self.browser.switch_to.window(curr)
            time.sleep(0.5)
            self.go_to_url(self.base_url)
        except:
            self.go_to_url(self.base_url)
        finally:
            self.wait_until_visible(By.ID, 'app-host', 30)
            
    def get_answer_code(self, key: str, string: str) -> str:
        """Get answer code for this or that quiz"""
        t = 0
        for i in range(len(string)):
            t += ord(string[i])
        t += int(key[-2:], 16)
        return str(t)
    
    def bing_searches(self, numberOfSearches: int, isMobile: bool = False):
        if not isMobile:
            self.section.emit("PC Bing Searches")
        else:
            self.section.emit("Mobile Bing Searches")
        self.detail.emit(f"0/{numberOfSearches}")
        i = 0
        try:
            r = RandomWords()
            search_terms = r.get_random_words(limit = numberOfSearches)
            if search_terms is None:
                raise Exception
        except Exception:
            search_terms = self.get_google_trends(numberOfSearches)
            if len(search_terms) == 0:
                try:
                    words = open(f"{Path(__file__).parent.resolve()}/searchwords.txt", "r").read().splitlines()
                    search_terms = random.sample(words, numberOfSearches)
                except:
                    raise GetSearchWordsException
        for word in search_terms:
            i += 1
            self.detail.emit(f"{i}/{numberOfSearches}")
            points = self.bing_search(word, isMobile)
            self.points.emit(points)
            if points <= self.points_counter :
                relatedTerms = self.get_related_terms(word)
                for term in relatedTerms :
                    points = self.bing_search(term, isMobile)
                    self.points.emit(points)
                    if points >= self.points_counter:
                        break
            if points > 0:
                self.points_counter = points
            else:
                break
            
    def bing_search(self, word: str, isMobile: bool):
        try:
            if not isMobile:
                self.browser.find_element(By.ID, 'sb_form_q').clear()
                time.sleep(1)
            else:
                self.go_to_url('https://bing.com')
        except:
            self.go_to_url('https://bing.com')
        time.sleep(2)
        searchbar = self.browser.find_element(By.ID, 'sb_form_q')
        if self.config["globalOptions"]["fast"]:
            searchbar.send_keys(word)
            time.sleep(1)
        else:
            for char in word:
                searchbar.send_keys(char)
                time.sleep(0.33)
        searchbar.submit()
        time.sleep(random.randint(12, 24) if not self.config["globalOptions"]["fast"] else random.randint(6, 9))
        points = 0
        try:
            points = self.get_points_from_bing(isMobile)
        except:
            pass
        return points
    
    def complete_promotional_items(self):
        try:
            self.detail.emit("Promotional items")
            item = self.get_dashboard_data()["promotionalItem"]
            if (item["pointProgressMax"] == 100 or item["pointProgressMax"] == 200) and item["complete"] == False and item["destinationUrl"] == self.base_url:
                self.browser.find_element(By.XPATH, '//*[@id="promo-item"]/section/div/div/div/a').click()
                time.sleep(1)
                self.browser.switch_to.window(window_name = self.browser.window_handles[1])
                time.sleep(8)
                self.points.emit(self.get_points_from_bing(False))
                self.browser.close()
                time.sleep(2)
                self.browser.switch_to.window(window_name = self.browser.window_handles[0])
                time.sleep(2)
        except:
            pass
        
    def complete_daily_set_search(self, cardNumber: int):
        time.sleep(5)
        self.browser.find_element(By.XPATH, f'//*[@id="app-host"]/ui-view/mee-rewards-dashboard/main/div/mee-rewards-daily-set-section/div/mee-card-group/div/mee-card[{str(cardNumber)}]/div/card-content/mee-rewards-daily-set-item-content/div/a/div/span').click()
        time.sleep(1)
        self.browser.switch_to.window(window_name = self.browser.window_handles[1])
        time.sleep(random.randint(13, 17) if not self.config["globalOptions"]["fast"] else random.randint(6, 9))
        self.points.emit(self.get_points_from_bing(False))
        self.browser.close()
        time.sleep(2)
        self.browser.switch_to.window(window_name = self.browser.window_handles[0])
        time.sleep(2)
        
    def complete_daily_set_survey(self, cardNumber: int):
        time.sleep(5)
        self.browser.find_element(By.XPATH, f'//*[@id="app-host"]/ui-view/mee-rewards-dashboard/main/div/mee-rewards-daily-set-section/div/mee-card-group/div/mee-card[{str(cardNumber)}]/div/card-content/mee-rewards-daily-set-item-content/div/a/div/span').click()
        time.sleep(1)
        self.browser.switch_to.window(window_name = self.browser.window_handles[1])
        time.sleep(8 if not self.config["globalOptions"]["fast"] else 5)
        # Accept cookie popup
        if self.is_element_exists(By.ID, 'bnp_container'):
            self.browser.find_element(By.ID, 'bnp_btn_accept').click()
            time.sleep(2)
        # Click on later on Bing wallpaper app popup
        if self.is_element_exists(By.ID, 'b_notificationContainer_bop'):
            self.browser.find_element(By.ID, 'bnp_hfly_cta2').click()
            time.sleep(2)
        self.browser.find_element(By.ID, "btoption" + str(random.randint(0, 1))).click()
        time.sleep(random.randint(10, 15) if not self.config["globalOptions"]["fast"] else 7)
        self.points.emit(self.get_points_from_bing(False))
        self.browser.close()
        time.sleep(2)
        self.browser.switch_to.window(window_name = self.browser.window_handles[0])
        time.sleep(2)
        
    def complete_daily_set_quiz(self, cardNumber: int):
        time.sleep(5)
        self.browser.find_element(By.XPATH, f'//*[@id="app-host"]/ui-view/mee-rewards-dashboard/main/div/mee-rewards-daily-set-section[1]/div/mee-card-group[1]/div[1]/mee-card[{str(cardNumber)}]/div[1]/card-content[1]/mee-rewards-daily-set-item-content[1]/div[1]/a[1]/div[3]/span[1]').click()
        time.sleep(3)
        self.browser.switch_to.window(window_name = self.browser.window_handles[1])
        time.sleep(12 if not self.config["globalOptions"]["fast"] else random.randint(5, 8))
        if not self.wait_until_quiz_loads():
            self.reset_tabs()
            return
        # Accept cookie popup
        if self.is_element_exists(By.ID, 'bnp_container'):
            self.browser.find_element(By.ID, 'bnp_btn_accept').click()
            time.sleep(2)
        self.browser.find_element(By.XPATH, '//*[@id="rqStartQuiz"]').click()
        self.wait_until_visible(By.XPATH, '//*[@id="currentQuestionContainer"]/div/div[1]', 10)
        time.sleep(3)
        numberOfQuestions = self.browser.execute_script("return _w.rewardsQuizRenderInfo.maxQuestions")
        numberOfOptions = self.browser.execute_script("return _w.rewardsQuizRenderInfo.numberOfOptions")
        for _ in range(numberOfQuestions):
            if numberOfOptions == 8:
                answers = []
                for i in range(8):
                    if self.browser.find_element(By.ID, "rqAnswerOption" + str(i)).get_attribute("iscorrectoption").lower() == "true":
                        answers.append("rqAnswerOption" + str(i))
                for answer in answers:
                    # Click on later on Bing wallpaper app popup
                    if self.is_element_exists(By.ID, 'b_notificationContainer_bop'):
                        self.browser.find_element(By.ID, 'bnp_hfly_cta2').click()
                        time.sleep(2)
                    self.browser.find_element(By.ID, answer).click()
                    time.sleep(5)
                    if not self.wait_until_question_refresh():
                        return
                time.sleep(5)
            elif numberOfOptions == 4:
                correctOption = self.browser.execute_script("return _w.rewardsQuizRenderInfo.correctAnswer")
                for i in range(4):
                    if self.browser.find_element(By.ID, "rqAnswerOption" + str(i)).get_attribute("data-option") == correctOption:
                        # Click on later on Bing wallpaper app popup
                        if self.is_element_exists(By.ID, 'b_notificationContainer_bop'):
                            self.browser.find_element(By.ID, 'bnp_hfly_cta2').click()
                            time.sleep(2)
                        self.browser.find_element(By.ID, "rqAnswerOption" + str(i)).click()
                        time.sleep(5)
                        if not self.wait_until_question_refresh():
                            return
                        break
                time.sleep(5)
            self.points.emit(self.get_points_from_bing(False))
        time.sleep(5)
        self.browser.close()
        time.sleep(2)
        self.browser.switch_to.window(window_name = self.browser.window_handles[0])
        time.sleep(2)

    def complete_daily_set_variable_activity(self, cardNumber: int):
        time.sleep(2)
        self.browser.find_element(By.XPATH, f'//*[@id="app-host"]/ui-view/mee-rewards-dashboard/main/div/mee-rewards-daily-set-section/div/mee-card-group/div/mee-card[{str(cardNumber)}]/div/card-content/mee-rewards-daily-set-item-content/div/a/div/span').click()
        time.sleep(1)
        self.browser.switch_to.window(window_name = self.browser.window_handles[1])
        time.sleep(8)
        # Accept cookie popup
        if self.is_element_exists(By.ID, 'bnp_container'):
            self.browser.find_element(By.ID, 'bnp_btn_accept').click()
            time.sleep(2)
        try :
            self.browser.find_element(By.XPATH, '//*[@id="rqStartQuiz"]').click()
            self.wait_until_visible(By.XPATH, '//*[@id="currentQuestionContainer"]/div/div[1]', 3)
        except (NoSuchElementException, TimeoutException):
            try:
                counter = str(self.browser.find_element(By.XPATH, '//*[@id="QuestionPane0"]/div[2]').get_attribute('innerHTML'))[:-1][1:]
                numberOfQuestions = max([int(s) for s in counter.split() if s.isdigit()])
                for question in range(numberOfQuestions):
                    # Click on later on Bing wallpaper app popup
                    if self.is_element_exists(By.ID, 'b_notificationContainer_bop'):
                        self.browser.find_element(By.ID, 'bnp_hfly_cta2').click()
                        time.sleep(2)
                        
                    self.browser.execute_script(f'document.evaluate("//*[@id=\'QuestionPane{str(question)}\']/div[1]/div[2]/a[{str(random.randint(1, 3))}]/div", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click()')
                    time.sleep(8)
                    self.points.emit(self.get_points_from_bing(False))
                time.sleep(5)
                self.browser.close()
                time.sleep(2)
                self.browser.switch_to.window(window_name=self.browser.window_handles[0])
                time.sleep(2)
                return
            except NoSuchElementException:
                time.sleep(random.randint(5, 9))
                self.browser.close()
                time.sleep(2)
                self.browser.switch_to.window(window_name = self.browser.window_handles[0])
                time.sleep(2)
                return
        time.sleep(3)
        correctAnswer = self.browser.execute_script("return _w.rewardsQuizRenderInfo.correctAnswer")
        if self.browser.find_element(By.ID, "rqAnswerOption0").get_attribute("data-option") == correctAnswer:
            self.browser.find_element(By.ID, "rqAnswerOption0").click()
        else :
            self.browser.find_element(By.ID, "rqAnswerOption1").click()
        time.sleep(10)
        self.points.emit(self.get_points_from_bing(False))
        self.browser.close()
        time.sleep(2)
        self.browser.switch_to.window(window_name = self.browser.window_handles[0])
        time.sleep(2)
        
    def complete_daily_set_this_or_that(self, cardNumber: int):
        time.sleep(2)
        self.browser.find_element(By.XPATH, f'//*[@id="app-host"]/ui-view/mee-rewards-dashboard/main/div/mee-rewards-daily-set-section/div/mee-card-group/div/mee-card[{str(cardNumber)}]/div/card-content/mee-rewards-daily-set-item-content/div/a/div/span').click()
        time.sleep(1)
        self.browser.switch_to.window(window_name=self.browser.window_handles[1])
        time.sleep(15 if not self.config["globalOptions"]["fast"] else random.randint(5, 8))
        # Accept cookie popup
        if self.is_element_exists(By.ID, 'bnp_container'):
            self.browser.find_element(By.ID, 'bnp_btn_accept').click()
            time.sleep(2)
        if not self.wait_until_quiz_loads():
            self.reset_tabs()
            return
        self.browser.find_element(By.XPATH, '//*[@id="rqStartQuiz"]').click()
        self.wait_until_visible(By.XPATH, '//*[@id="currentQuestionContainer"]/div/div[1]', 10)
        time.sleep(5)
        for _ in range(10):
            # Click on later on Bing wallpaper app popup
            if self.is_element_exists(By.ID, 'b_notificationContainer_bop'):
                self.browser.find_element(By.ID, 'bnp_hfly_cta2').click()
                time.sleep(2)
            
            answerEncodeKey = self.browser.execute_script("return _G.IG")

            answer1 = self.browser.find_element(By.ID, "rqAnswerOption0")
            answer1Title = answer1.get_attribute('data-option')
            answer1Code = self.get_answer_code(answerEncodeKey, answer1Title)

            answer2 = self.browser.find_element(By.ID, "rqAnswerOption1")
            answer2Title = answer2.get_attribute('data-option')
            answer2Code = self.get_answer_code(answerEncodeKey, answer2Title)

            correctAnswerCode = self.browser.execute_script("return _w.rewardsQuizRenderInfo.correctAnswer")

            if (answer1Code == correctAnswerCode):
                answer1.click()
                time.sleep(15 if not self.config["globalOptions"]["fast"] else 7)
            elif (answer2Code == correctAnswerCode):
                answer2.click()
                time.sleep(15 if not self.config["globalOptions"]["fast"] else 7)
            self.points.emit(self.get_points_from_bing(False))

        time.sleep(5)
        self.browser.close()
        time.sleep(2)
        self.browser.switch_to.window(window_name=self.browser.window_handles[0])
        time.sleep(2)
    
    def complete_daily_set(self):
        self.section.emit("Daily Set")
        d = self.get_dashboard_data()
        todayDate = datetime.today().strftime('%m/%d/%Y')
        todayPack = []
        for date, data in d['dailySetPromotions'].items():
            if date == todayDate:
                todayPack = data
        for activity in todayPack:
            try:
                if activity['complete'] == False:
                    cardNumber = int(activity['offerId'][-1:])
                    if activity['promotionType'] == "urlreward":
                        self.detail.emit(f'Search of card {str(cardNumber)}')
                        self.complete_daily_set_search(cardNumber)
                    if activity['promotionType'] == "quiz":
                        if activity['pointProgressMax'] == 50 and activity['pointProgress'] == 0:
                            self.detail.emit(f'This or That of card {str(cardNumber)}')
                            self.complete_daily_set_this_or_that(cardNumber)
                        elif (activity['pointProgressMax'] == 40 or activity['pointProgressMax'] == 30) and activity['pointProgress'] == 0:
                            self.detail.emit(f"Quiz of card {str(cardNumber)}")
                            self.complete_daily_set_quiz(cardNumber)
                        elif activity['pointProgressMax'] == 10 and activity['pointProgress'] == 0:
                            searchUrl = urllib.parse.unquote(urllib.parse.parse_qs(urllib.parse.urlparse(activity['destinationUrl']).query)['ru'][0])
                            searchUrlQueries = urllib.parse.parse_qs(urllib.parse.urlparse(searchUrl).query)
                            filters = {}
                            for filter in searchUrlQueries['filters'][0].split(" "):
                                filter = filter.split(':', 1)
                                filters[filter[0]] = filter[1]
                            if "PollScenarioId" in filters:
                                self.detail.emit(f"Poll of card {str(cardNumber)}")
                                self.complete_daily_set_survey(cardNumber)
                            else:
                                self.detail.emit(f"Quiz of card {str(cardNumber)}")
                                self.complete_daily_set_variable_activity(cardNumber)
            except Exception as e:
                self.save_error(e)
                self.reset_tabs()
        self.logs[self.current_account]['Daily'] = True
        self.update_logs() 
        
    def complete_punch_card(self, url: str, childPromotions: dict):
        self.go_to_url(url)
        for child in childPromotions:
            if child['complete'] == False:
                if child['promotionType'] == "urlreward":
                    self.browser.execute_script("document.getElementsByClassName('offer-cta')[0].click()")
                    time.sleep(1)
                    self.browser.switch_to.window(window_name = self.browser.window_handles[1])
                    time.sleep(random.randint(13, 17))
                    self.browser.close()
                    time.sleep(2)
                    self.browser.switch_to.window(window_name = self.browser.window_handles[0])
                    time.sleep(2)
                if child['promotionType'] == "quiz" and child['pointProgressMax'] >= 50 :
                    self.browser.find_element(By.XPATH, '//*[@id="rewards-dashboard-punchcard-details"]/div[2]/div[2]/div[7]/div[3]/div[1]/a').click()
                    time.sleep(1)
                    self.browser.switch_to.window(window_name = self.browser.window_handles[1])
                    time.sleep(15)
                    try:
                        self.browser.find_element(By.XPATH, '//*[@id="rqStartQuiz"]').click()
                    except:
                        pass
                    time.sleep(5)
                    self.wait_until_visible(By.XPATH, '//*[@id="currentQuestionContainer"]', 10)
                    numberOfQuestions = self.browser.execute_script("return _w.rewardsQuizRenderInfo.maxQuestions")
                    AnswerdQuestions = self.browser.execute_script("return _w.rewardsQuizRenderInfo.CorrectlyAnsweredQuestionCount")
                    numberOfQuestions -= AnswerdQuestions
                    for question in range(numberOfQuestions):
                        answer = self.browser.execute_script("return _w.rewardsQuizRenderInfo.correctAnswer")
                        self.browser.find_element(By.XPATH, f'//input[@value="{answer}"]').click()
                        time.sleep(15)
                    time.sleep(5)
                    self.browser.close()
                    time.sleep(2)
                    self.browser.switch_to.window(window_name=self.browser.window_handles[0])
                    time.sleep(2)
                    self.browser.refresh()
                    break
                elif child['promotionType'] == "quiz" and child['pointProgressMax'] < 50:
                    self.browser.execute_script("document.getElementsByClassName('offer-cta')[0].click()")
                    time.sleep(1)
                    self.browser.switch_to.window(window_name = self.browser.window_handles[1])
                    time.sleep(8)
                    counter = str(self.browser.find_element(By.XPATH, '//*[@id="QuestionPane0"]/div[2]').get_attribute('innerHTML'))[:-1][1:]
                    numberOfQuestions = max([int(s) for s in counter.split() if s.isdigit()])
                    for question in range(numberOfQuestions):
                        self.browser.execute_script('document.evaluate("//*[@id=\'QuestionPane' + str(question) + '\']/div[1]/div[2]/a[' + str(random.randint(1, 3)) + ']/div", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click()')
                        time.sleep(10)
                    time.sleep(5)
                    self.browser.close()
                    time.sleep(2)
                    self.browser.switch_to.window(window_name = self.browser.window_handles[0])
                    time.sleep(2)
                    self.browser.refresh()
                    break
                
    def complete_punch_cards(self):
        punchCards = self.get_dashboard_data()['punchCards']
        self.section.emit("Punch cards")
        self.detail.emit("-")
        for punchCard in punchCards:
            try:
                if punchCard['parentPromotion'] != None and punchCard['childPromotions'] != None and punchCard['parentPromotion']['complete'] == False and punchCard['parentPromotion']['pointProgressMax'] != 0:
                    url = punchCard['parentPromotion']['attributes']['destination']
                    if self.browser.current_url.startswith('https://rewards.'):
                        path = url.replace('https://rewards.microsoft.com', '')
                        new_url = 'https://rewards.microsoft.com/dashboard/'
                        userCode = path[11:15]
                        dest = new_url + userCode + path.split(userCode)[1]
                    else:
                        path = url.replace('https://account.microsoft.com/rewards/dashboard/','')
                        new_url = 'https://account.microsoft.com/rewards/dashboard/'
                        userCode = path[:4]
                        dest = new_url + userCode + path.split(userCode)[1]
                    self.complete_punch_card(url, punchCard['childPromotions'])
            except Exception as e:
                self.save_error(e)
                self.reset_tabs()
        time.sleep(2)
        self.go_to_url(self.base_url)
        time.sleep(2)
        self.logs[self.current_account]['Punch cards'] = True
        self.update_logs()
        self.wait_until_visible(By.ID, 'app-host', 30)
        
    def complete_more_promotion_search(self, destinationUrl: str):
        self.browser.execute_script("window.open();")
        time.sleep(1)
        self.browser.switch_to.window(window_name = self.browser.window_handles[1])
        self.go_to_url(destinationUrl)
        time.sleep(random.randint(13, 17) if not self.config["globalOptions"]["fast"] else random.randint(5, 8))
        self.points.emit(self.get_points_from_bing(False))
        self.browser.close()
        time.sleep(2)
        self.browser.switch_to.window(window_name = self.browser.window_handles[0])
        time.sleep(2)
        
    def complete_more_promotion_quiz(self, destinationUrl: str):
        self.browser.execute_script("window.open();")
        time.sleep(1)
        self.browser.switch_to.window(window_name=self.browser.window_handles[1])
        self.go_to_url(destinationUrl)
        time.sleep(8 if not self.config["globalOptions"] else 5)
        if not self.wait_until_quiz_loads():
            self.reset_tabs()
            return
        CurrentQuestionNumber = self.browser.execute_script("return _w.rewardsQuizRenderInfo.currentQuestionNumber")
        if CurrentQuestionNumber == 1 and self.is_element_exists(By.XPATH, '//*[@id="rqStartQuiz"]'):
            self.browser.find_element(By.XPATH, '//*[@id="rqStartQuiz"]').click()
        self.wait_until_visible(By.XPATH, '//*[@id="currentQuestionContainer"]/div/div[1]', 10)
        time.sleep(3)
        numberOfQuestions = self.browser.execute_script("return _w.rewardsQuizRenderInfo.maxQuestions")
        Questions = numberOfQuestions - CurrentQuestionNumber + 1
        numberOfOptions = self.browser.execute_script("return _w.rewardsQuizRenderInfo.numberOfOptions")
        for _ in range(Questions):
            if numberOfOptions == 8:
                answers = []
                for i in range(8):
                    if self.browser.find_element(By.ID, "rqAnswerOption" + str(i)).get_attribute("iscorrectoption").lower() == "true":
                        answers.append("rqAnswerOption" + str(i))
                for answer in answers:
                    self.browser.find_element(By.ID, answer).click()
                    time.sleep(5)
                    if not self.wait_until_question_refresh():
                        return
                time.sleep(5)
            elif numberOfOptions == 4:
                correctOption = self.browser.execute_script("return _w.rewardsQuizRenderInfo.correctAnswer")
                for i in range(4):
                    if self.browser.find_element(By.ID, "rqAnswerOption" + str(i)).get_attribute("data-option") == correctOption:
                        self.browser.find_element(By.ID, "rqAnswerOption" + str(i)).click()
                        time.sleep(5)
                        if not self.wait_until_question_refresh():
                            return
                        break
                time.sleep(5)
            self.points.emit(self.get_points_from_bing(False))
        time.sleep(5)
        self.browser.close()
        time.sleep(2)
        self.browser.switch_to.window(window_name=self.browser.window_handles[0])
        time.sleep(2)
        
    def complete_more_promotion_ABC(self, destinationUrl: str):
        self.browser.execute_script("window.open();")
        time.sleep(1)
        self.browser.switch_to.window(window_name=self.browser.window_handles[1])
        self.go_to_url(destinationUrl)
        time.sleep(8 if not self.config["globalOptions"]["fast"] else 5)
        counter = str(self.browser.find_element(By.XPATH, '//*[@id="QuestionPane0"]/div[2]').get_attribute('innerHTML'))[:-1][1:]
        numberOfQuestions = max([int(s) for s in counter.split() if s.isdigit()])
        for question in range(numberOfQuestions):
            self.browser.execute_script(f'document.evaluate("//*[@id=\'QuestionPane{str(question)}\']/div[1]/div[2]/a[{str(random.randint(1, 3))}]/div", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click()')
            time.sleep(8 if not self.config["globalOptions"]["fast"] else 5)
        time.sleep(5)
        self.points.emit(self.get_points_from_bing(False))
        self.browser.close()
        time.sleep(2)
        self.browser.switch_to.window(window_name=self.browser.window_handles[0])
        time.sleep(2)
        
    def complete_more_promotion_this_or_that(self, destinationUrl: str):
        self.browser.execute_script("window.open();")
        time.sleep(1)
        self.browser.switch_to.window(window_name=self.browser.window_handles[1])
        self.go_to_url(destinationUrl)
        time.sleep(8 if not self.config["globalOptions"]["fast"] else 5)
        if not self.wait_until_quiz_loads():
            self.reset_tabs()
            return
        CrrentQuestionNumber = self.browser.execute_script("return _w.rewardsQuizRenderInfo.currentQuestionNumber")
        NumberOfQuestionsLeft = 10 - CrrentQuestionNumber + 1
        if CrrentQuestionNumber == 1 and self.is_element_exists(By.XPATH, '//*[@id="rqStartQuiz"]'):
            self.browser.find_element(By.XPATH, '//*[@id="rqStartQuiz"]').click()
        self.wait_until_visible(By.XPATH, '//*[@id="currentQuestionContainer"]/div/div[1]', 10)
        time.sleep(3)
        for _ in range(NumberOfQuestionsLeft):
            answerEncodeKey = self.browser.execute_script("return _G.IG")

            answer1 = self.browser.find_element(By.ID, "rqAnswerOption0")
            answer1Title = answer1.get_attribute('data-option')
            answer1Code = self.get_answer_code(answerEncodeKey, answer1Title)

            answer2 = self.browser.find_element(By.ID, "rqAnswerOption1")
            answer2Title = answer2.get_attribute('data-option')
            answer2Code = self.get_answer_code(answerEncodeKey, answer2Title)

            correctAnswerCode = self.browser.execute_script("return _w.rewardsQuizRenderInfo.correctAnswer")

            if (answer1Code == correctAnswerCode):
                answer1.click()
                time.sleep(8 if not self.config["globalOptions"]["fast"] else 5)
            elif (answer2Code == correctAnswerCode):
                answer2.click()
                time.sleep(8 if not self.config["globalOptions"]["fast"] else 5)
            self.points.emit(self.get_points_from_bing(False))

        time.sleep(5)
        self.browser.close()
        time.sleep(2)
        self.browser.switch_to.window(window_name=self.browser.window_handles[0])
        time.sleep(2)
        
    def complete_more_promotions(self):
        self.section.emit("More activities")
        morePromotions = self.get_dashboard_data()['morePromotions']
        i = 0
        for promotion in morePromotions:
            try:
                i += 1
                if promotion['complete'] == False and promotion['pointProgressMax'] != 0:
                    if promotion['promotionType'] == "urlreward":
                        self.detail.emit("Search card")
                        self.complete_more_promotion_search(promotion["destinationUrl"])
                    elif promotion['promotionType'] == "quiz":
                        if promotion['pointProgressMax'] == 10:
                            self.detail.emit("ABC card")
                            self.complete_more_promotion_ABC(promotion["destinationUrl"])
                        elif promotion['pointProgressMax'] == 30 or promotion['pointProgressMax'] == 40:
                            self.detail.emit("Quiz card")
                            self.complete_more_promotion_quiz(promotion["destinationUrl"])
                        elif promotion['pointProgressMax'] == 50:
                            self.detail.emit("This or that card")
                            self.complete_more_promotion_this_or_that(promotion["destinationUrl"])
                    else:
                        if promotion['pointProgressMax'] == 100 or promotion['pointProgressMax'] == 200:
                            self.detail.emit("Search card")
                            self.complete_more_promotion_search(promotion["destinationUrl"])
                if promotion['complete'] == False and promotion['pointProgressMax'] == 100 and promotion['promotionType'] == "" \
                    and promotion['destinationUrl'] == self.base_url:
                        self.detail.emit("Search card")
                        self.complete_more_promotion_search(promotion["destinationUrl"])
            except Exception as e:
                self.save_error(e)
                self.reset_tabs()
        self.logs[self.current_account]['More promotions'] = True
        self.update_logs()
    
    def complete_msn_shopping_game_quiz(self):

        def expand_shadow_element(element, index: int = None) -> Union[List[WebElement], WebElement]:
            """Returns childrens of shadow element"""
            if index is not None:
                shadow_root = WebDriverWait(self.browser, 45).until(
                    ec.visibility_of(self.browser.execute_script('return arguments[0].shadowRoot.children', element)[index])
                )
            else:
                # wait to visible one element then get the list
                WebDriverWait(self.browser, 45).until(
                    ec.visibility_of(self.browser.execute_script('return arguments[0].shadowRoot.children', element)[0])
                )
                shadow_root = self.browser.execute_script('return arguments[0].shadowRoot.children', element)
            return shadow_root

        def get_children(element) -> List[WebElement]:
            children = self.browser.execute_script('return arguments[0].children', element)
            return children
        
        def get_sign_in_button() -> WebElement:
            """return the sign in button"""
            script_to_user_pref_container = 'document.getElementsByTagName("shopping-page-base")[0]\
                .shadowRoot.children[0].children[1].children[0]\
                .shadowRoot.children[0].shadowRoot.children[0]\
                .getElementsByClassName("user-pref-container")[0]'
            WebDriverWait(self.browser, 60).until(ec.visibility_of(
                self.browser.execute_script(f'return {script_to_user_pref_container}')
                )
            )
            button = WebDriverWait(self.browser, 60).until(ec.visibility_of(
                    self.browser.execute_script(
                        f'return {script_to_user_pref_container}.\
                        children[0].children[0].shadowRoot.children[0].\
                        getElementsByClassName("me-control")[0]'
                    )
                )
            )
            return button
            
        def sign_in():
            self.ui.update_detail("Signing in")
            sign_in_button = get_sign_in_button()
            sign_in_button.click()
            time.sleep(5)
            self.wait_until_visible(By.ID, 'newSessionLink', 10)
            self.browser.find_element(By.ID, 'newSessionLink').click()
            self.answer_to_security_info_update()
            self.ui.update_detail("Waiting for elements")
            self.wait_until_visible(By.TAG_NAME, 'shopping-page-base', 60)
            expand_shadow_element(self.browser.find_element(By.TAG_NAME, 'shopping-page-base'), 0)
            self.ui.update_detail("Checking signed in state")
            get_sign_in_button()
        
        def get_gaming_card() -> Union[WebElement, bool]:
            shopping_page_base_childs = expand_shadow_element(self.browser.find_element(By.TAG_NAME, 'shopping-page-base'), 0)
            shopping_homepage = shopping_page_base_childs.find_element(By.TAG_NAME, 'shopping-homepage')
            msft_feed_layout = expand_shadow_element(shopping_homepage, 0).find_element(By.TAG_NAME, 'msft-feed-layout')
            msn_shopping_game_pane = expand_shadow_element(msft_feed_layout)
            for element in msn_shopping_game_pane:
                if element.get_attribute("gamestate") == "active":
                    return element
            else:
                return False
        
        def click_correct_answer():
            options_container = expand_shadow_element(gaming_card, 1)
            options_elements = get_children(get_children(options_container)[1])
            # click on the correct answer in options_elements
            correct_answer = options_elements[int(gaming_card.get_attribute("_correctAnswerIndex"))]
            # hover to show the select button
            correct_answer.click()
            time.sleep(1)
            # click 'select' button
            select_button = correct_answer.find_element(By.CLASS_NAME, 'shopping-select-overlay-button')
            WebDriverWait(self.browser, 5).until(ec.element_to_be_clickable(select_button))
            select_button.click()
        
        def click_play_again():
            time.sleep(random.randint(4, 6))
            options_container = expand_shadow_element(gaming_card)[1]
            get_children(options_container)[0].find_element(By.TAG_NAME, 'button').click()
        
        try:
            tries = 0
            while tries <= 4:
                tries += 1
                self.ui.update_section(f"MSN quiz - try ({tries})")
                self.go_to_url("https://www.msn.com/en-us/shopping")
                self.ui.update_detail("Waiting for elements")
                self.wait_until_visible(By.TAG_NAME, 'shopping-page-base', 60)
                time.sleep(15)
                self.ui.update_detail("Waiting for sign in state")
                try:
                    sign_in_button = get_sign_in_button()
                except:
                    if tries == 4:
                        raise ElementNotVisibleException("Sign in button did not show up")
                else:
                    break
            time.sleep(5)
            if "Sign in" in sign_in_button.text:
                sign_in()
            self.ui.update_detail("Locating gaming card")
            gaming_card = get_gaming_card()
            scrolls = 0
            while not gaming_card and scrolls <= 5:
                scrolls += 1
                self.ui.update_detail(f"Locating gaming card")
                self.browser.execute_script("window.scrollBy(0, 300);")
                time.sleep(10)
                gaming_card = get_gaming_card()
                if gaming_card:
                    self.browser.execute_script("arguments[0].scrollIntoView();", gaming_card)
                    self.ui.update_detail("Gaming card found")
                    time.sleep(random.randint(7, 10))
                if scrolls == 5 and not gaming_card:
                    raise GamingCardNotFound("Gaming card not found")
            self.ui.update_detail("Answering questions")
            for _ in range(10):
                try:
                    click_correct_answer()
                    click_play_again()
                    time.sleep(random.randint(5, 7))
                except (NoSuchElementException, JavascriptException):
                    break
        except GamingCardNotFound:
            self.ui.update_detail("Gaming card not found")
        except Exception as e:
            self.save_error(e)
            self.ui.update_detail("Failed to complete")
        else:
            self.ui.update_detail("Completed")
        finally:
            self.logs[self.current_account]["MSN shopping game"] = True
            self.go_to_url(self.base_url)
            self.update_logs()
            self.ui.update_section("-")
            self.ui.update_detail("-")
            self.wait_until_visible(By.ID, 'app-host', 30)
      
    def get_remaining_searches(self):
        dashboard = self.get_dashboard_data()
        searchPoints = 1
        counters = dashboard['userStatus']['counters']
        if not 'pcSearch' in counters:
            return 0, 0
        progressDesktop = counters['pcSearch'][0]['pointProgress'] + counters['pcSearch'][1]['pointProgress']
        targetDesktop = counters['pcSearch'][0]['pointProgressMax'] + counters['pcSearch'][1]['pointProgressMax']
        if targetDesktop == 33 :
            #Level 1 EU
            searchPoints = 3
        elif targetDesktop == 55 :
            #Level 1 US
            searchPoints = 5
        elif targetDesktop == 102 :
            #Level 2 EU
            searchPoints = 3
        elif targetDesktop >= 170 :
            #Level 2 US
            searchPoints = 5
        remainingDesktop = int((targetDesktop - progressDesktop) / searchPoints)
        remainingMobile = 0
        if dashboard['userStatus']['levelInfo']['activeLevel'] != "Level1":
            progressMobile = counters['mobileSearch'][0]['pointProgress']
            targetMobile = counters['mobileSearch'][0]['pointProgressMax']
            remainingMobile = int((targetMobile - progressMobile) / searchPoints)
        return remainingDesktop, remainingMobile
    
    def get_points_from_bing(self, isMobile: bool = False):
        try:
            if not isMobile:
                try:
                    points = int(self.browser.find_element(By.ID, 'id_rc').get_attribute('innerHTML'))
                except ValueError:
                    points = int(self.browser.find_element(By.ID, 'id_rc').get_attribute('innerHTML').replace(",", ""))
            else:
                try:
                    self.browser.find_element(By.ID, 'mHamburger').click()
                except UnexpectedAlertPresentException:
                    try:
                        self.browser.switch_to.alert.accept()
                        time.sleep(1)
                        self.browser.find_element(By.ID, 'mHamburger').click()
                    except NoAlertPresentException:
                        pass
                time.sleep(1)
                points = int(self.browser.find_element(By.ID, 'fly_id_rc').get_attribute('innerHTML'))
        except NoSuchElementException:
            points = self.points_counter
        return points
    
    def save_error(self, e):
        if self.config["globalOptions"]["saveErrors"]:
            with open(f"{Path.cwd()}/errors.txt", "a") as f:
                f.write(f"\n-------------------{datetime.now()}-------------------\r\n")
                f.write(f"{str(e)}\n")
    
    def perform_run(self):
        """Check whether timer is set to run it at time else run immediately"""
        if self.ui.get_timer_checkbox_status():
            self.stop_button_enabled.emit(True)
            requested_time = self.ui.get_time().toString("HH:mm")
            self.section.emit(f"Starts at {requested_time}")
            while QTime.currentTime().toString("HH:mm") != requested_time:
                if self.ui.farmer_thread.isInterruptionRequested():
                    self.finished.emit()
                    return None
                time.sleep(1)
            else:
                self.get_or_create_logs()
                return self.run()
        self.get_or_create_logs()
        return self.run()
    
    def run(self):
        start = time.time()
        for account in self.accounts:
            while True:
                try:
                    self.current_account = account["username"]
                    if account["username"] in self.finished_accounts or account["username"] in self.suspended_accounts:
                        break
                    if self.logs[self.current_account]["Last check"] != str(date.today()):
                        self.logs[self.current_account]["Last check"] = str(date.today())
                        self.update_logs()
                    self.accounts_info_sig.emit()
                    if self.is_pc_need():

                        self.browser_setup(False, self.PC_USER_AGENT, account.get("proxy", None))
                        self.stop_button_enabled.emit(True)
                        self.login(account["username"], account["password"])
                        self.detail.emit("Logged in")
                        
                        self.go_to_url(self.base_url)
                        self.wait_until_visible(By.ID, 'app-host', 30)
                        self.starting_points = self.get_account_points()
                        redeem_goal_title, redeem_goal_price = self.get_redeem_goal()
                        self.points_counter = self.starting_points
                        self.points.emit(self.points_counter)

                        if self.config["farmOptions"]["dailyQuests"] and not self.logs[self.current_account]["Daily"]:
                            self.complete_daily_set()
                            self.points.emit(self.points_counter)

                        if self.config["farmOptions"]["punchCards"] and not self.logs[self.current_account]["Punch cards"]:
                            self.complete_punch_cards()
                            self.points.emit(self.points_counter)

                        if self.config["farmOptions"]["moreActivities"] and not self.logs[self.current_account]["More promotions"]:
                            self.complete_more_promotions()
                            self.points.emit(self.points_counter)
                            
                        if self.config["farmOptions"]["MSNShoppingGame"] and not self.logs[self.current_account]["MSN shopping game"]:
                            self.complete_msn_shopping_game_quiz()
                            self.points_counter = self.get_account_points()
                            self.points.emit(self.points_counter)

                        if self.config["farmOptions"]["searchPC"] and not self.logs[self.current_account]["PC searches"]:
                            remainingSearches = self.get_remaining_searches()[0]
                            self.bing_searches(remainingSearches)
                            self.logs[self.current_account]["PC searches"] = True
                            self.update_logs()
                            
                        self.stop_button_enabled.emit(False)
                        self.browser.quit()
                        self.detail.emit("-")
                        self.section.emit("-")
                        

                    if self.config["farmOptions"]["searchMobile"] and not self.logs[self.current_account]["Mobile searches"]:
                        self.browser_setup(True, account.get('mobile_user_agent', self.MOBILE_USER_AGENT), account.get("proxy", None))
                        self.stop_button_enabled.emit(True)
                        self.login(account["username"], account["password"], True)
                        self.go_to_url(self.base_url)
                        self.wait_until_visible(By.ID, 'app-host', 30)
                        redeem_goal_title, redeem_goal_price = self.get_redeem_goal()
                        if not self.starting_points:
                            self.starting_points = self.get_account_points()
                        remainingSearches = self.get_remaining_searches()[1]
                        if remainingSearches > 0:
                            self.bing_searches(remainingSearches, True)
                        self.logs[self.current_account]["Mobile searches"] = True
                        self.update_logs()
                        self.stop_button_enabled.emit(False)
                        self.browser.quit()
                        self.detail.emit("-")
                        self.section.emit("-")

                    self.finished_accounts.append(account["username"])
                    if self.logs[self.current_account]["Points"] > 0 and self.points_counter >= self.logs[self.current_account]["Points"] :
                        self.logs[self.current_account]["Today's points"] = self.points_counter - self.logs[self.current_account]["Points"]
                    else:
                        self.logs[self.current_account]["Today's points"] = self.points_counter - self.starting_points
                    self.logs[account["username"]]["Points"] = self.points_counter
                    
                    if self.config["telegram"]["sendToTelegram"] and redeem_goal_title != "" and redeem_goal_price <= self.points_counter:
                        self.logs[self.current_account]["Redeem goal title"] = redeem_goal_title
                        self.logs[self.current_account]["Redeem goal price"] = redeem_goal_price
                        
                    self.clean_logs()
                    self.update_logs()

                    self.points.emit(0)
                    self.accounts_info_sig.emit()
                    
                    break
                    
                except SessionNotCreatedException:
                    self.browser = None
                    self.section.emit("Update your web driver")
                    self.detail.emit("web driver error")
                    self.finished.emit()
                    return None
                
                except AccountLockedException:
                    self.browser.quit()
                    self.browser = None
                    self.logs[self.current_account]['Last check'] = 'Your account has been locked !'
                    self.locked_accounts.append(self.current_account)
                    self.update_logs()
                    self.clean_logs()
                    break
                
                except AccountSuspendedException:
                    self.browser.quit()
                    self.browser = None
                    self.suspended_accounts.append(self.current_account)
                    self.logs[self.current_account]['Last check'] = 'Your account has been suspended'
                    self.logs[self.current_account]["Today's points"] = 'N/A' 
                    self.logs[self.current_account]["Points"] = 'N/A' 
                    self.clean_logs()
                    self.update_logs()
                    self.finished_accounts.append(self.current_account)
                    self.accounts_info_sig.emit()
                    break
                
                except UnusualActivityException:
                    self.browser.quit()
                    self.browser = None
                    self.logs[self.current_account]['Last check'] = 'Unusual activity detected !'
                    self.finished_accounts.append(self.current_account)       
                    self.update_logs()
                    self.clean_logs()
                    self.finished.emit()
                    return None
                    
                except RegionException:
                    self.browser.quit()
                    self.browser = None
                    self.finished.emit()
                    self.section.emit("Not available in your region")
                    return None
                
                except GetSearchWordsException:
                    self.browser.quit()
                    self.browser = None
                    self.logs[self.current_account]['Last check'] = "Couldn't get search words"
                    self.finished_accounts.append(self.current_account)       
                    self.update_logs()
                    self.clean_logs()
                    self.accounts_info_sig.emit()
                    break
                
                except UnhandledException:
                    self.browser.quit()
                    self.browser = None
                    self.logs[self.current_account]['Last check'] = 'Unknown error !'
                    self.finished_accounts.append(self.current_account)
                    self.update_logs()
                    self.clean_logs()
                    break
                
                except (InvalidSessionIdException, MaxRetryError, NewConnectionError):
                    if isinstance(self.browser, WebDriver):
                        self.browser.quit()
                        self.browser = None
                    if self.ui.farmer_thread.isInterruptionRequested():
                        self.finished.emit()
                        self.ui.enable_elements()
                        return None
                    internet = self.check_internet_connection()
                    if internet:
                        pass
                    else:
                        self.finished.emit()
                        self.ui.enable_elements()
                        return None
                    
                except (Exception, FunctionTimedOut) as e:
                    if isinstance(self.browser, WebDriver):
                        self.browser.quit()
                    self.starting_points = None
                    self.browser = None
                    self.save_error(e)
                    internet = self.check_internet_connection()
                    if internet:
                        pass
                    else:
                        self.finished.emit()
                        self.ui.enable_elements()
                        return None
                    
        else:
            if self.config["telegram"]["sendToTelegram"]:
                message = self.create_message()
                self.send_report_to_telegram(message)
            end = time.time()
            delta = end - start
            hour, remain = divmod(delta, 3600)
            min, sec = divmod(remain, 60)
            self.logs["Elapsed time"] = f"{hour:02.0f}:{min:02.0f}:{sec:02.0f}"
            self.update_logs()
            if self.config["globalOptions"]["shutdownSystem"]: os.system("shutdown /s /t 10")
            self.finished.emit()
        