<h2 align="center">Microsoft Rewards Bot</h2>

<p align="center">
  <img src="https://forthebadge.com/images/badges/made-with-python.svg"/>
  <img src="http://ForTheBadge.com/images/badges/built-by-developers.svg"/>
  <img src="http://ForTheBadge.com/images/badges/uses-git.svg"/>
  <img src="http://ForTheBadge.com/images/badges/built-with-love.svg"/>
  <a href="https://discord.gg/GaF8fFBtE3"><img src="https://dcbadge.vercel.app/api/server/GaF8fFBtE3"/></a>
</p>
<h3 align="center">A simple script that uses Selenium and PyQt5 to farm Microsoft Rewards.</h3>
<details align="center">
  <summary>
    <h3>⭐️<a href="https://github.com/farshadz1997/Microsoft-Rewards-bot-GUI-V2">New GUI version</a> with more features available, GUI powered by Flet framework⭐️</h3>
  </summary>
  <img src="https://user-images.githubusercontent.com/60227955/218319443-3f5ea317-e759-4e4c-a847-926b240e2806.png" alt="Microsoft Rewards bot GUI-V2"</img>
</details>
<br>
<p align="center">
<img src="https://user-images.githubusercontent.com/60227955/206023577-f933334c-edf3-49fe-b30e-12d806847ab7.png"</img>
</p>

## Installation
<p align="left">
  <ul>
    <li>Install requirements with the following command : <pre>pip install -r requirements.txt</pre></li>
    <li>Make sure you have Chrome installed</li>
    <li>Install ChromeDriver :<ul>
      <li>Windows :<ul>
        <li>Download Chrome WebDriver as same as your Google Chrome version : https://chromedriver.chromium.org/downloads</li>
        <li>Place the file in X:\Windows (X as your Windows disk letter)</li>
      </ul>
      <li>MacOS or Linux :<ul>
        <li><pre>apt install chromium-chromedriver</pre></li>
        <li>or if you have brew : <pre>brew cask install chromedriver</pre></li>
      </ul>
    </ul></li>
    <li>Edit the accounts.json.sample with your accounts credentials and rename it by removing .sample at the end<br/>
    If you want to add more than one account, the syntax is the following (<code>mobile_user_agent</code> is optional): <pre>[
    {
        "username": "Your Email",
        "password": "Your Password",
        "mobile_user_agent": "your preferred mobile user agent", # (remove this line if you don't want to use mobile user agent)
        "proxy": "HTTP proxy" # (remove this line if you don't want to use proxy)
    },
    {
        "username": "Your Email 2",
        "password": "Your Password 2",
        "mobile_user_agent": "your preferred mobile user agent", # (remove this line if you don't want to use mobile user agent)
        "proxy": "HTTP proxy2" (remove this line if you don't want to use proxy)
    }
]</pre></li>
  <li>Run main.pyw</li>
  </ul>
</p>

## Features
<p align="left">
  <ul>
    <li>Save progress of accounts in logs</li>
    <li>Use headless to create browser instance without graphical user interface (Not recommended)</li>
    <li>You can use fast mode to redeuce delays in bot if you have high speed Internet connection</li>
    <li>Save errors in a txt file for any unpredicted errors</li>
    <li>You can save your account in browser session by enabling it to skip login on each start</li>
    <li>You can choose to farm which part of Microsoft Rewards among daily quests, punch cards, more activities, MSN shopping game, Bing searches (PC, Mobile)</li>
    <li>You can set time for bot to start at your desired time</li>
    <li>Supports HTTP proxy for each account</li>
    <li>Send logs to your Telegram through your Telegram bot with setting Token and Chat ID</li>
    <li>Shutdown PC after farm</li>
   </ul>
</p>

## Support me
Your support will be much appreciated

  - <b>BTC (BTC network):</b> bc1qn52jx934nd54vhcv6x5xxsrc7z2qvwf6atcut3
  - <b>ETH (ERC20):</b> 0x2486D75EC2675833569b85d77b01C2c37097ECc2
  - <b>LTC:</b> ltc1qc03mnemxewn6z0chfc20yw4samucg6kczmwuf8
  - <b>USDT (ERC20):</b> 0x2486D75EC2675833569b85d77b01C2c37097ECc2


## Credits
Core script created by [@charlesbel](https://github.com/charlesbel/Microsoft-Rewards-Farmer).
